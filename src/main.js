const codegen = require('./codegen/index.js');
const js2uml = require('./js2uml/index.js');
const build = require('./builder/index.js')
let build4code = {
  "codegen": codegen,
  "js2uml": js2uml,
  "build": build,
};

// -------NPM Export Variable: Handlebars4Code---------------
module.exports = build4code;
