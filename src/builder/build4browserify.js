// WTF BUILDER
// this is a build script extracted an modified from wtf_wikipedia
/*
 Acknowledgement:
the build script was integrated into build4code so that contributions
to the Wiki Transformation packages of Spencer Kelly  spencermountain@GitHub
can use the build script with NPM

Required packages:
- terser  (i.e. uglify-es fork)
- browserify (to create libraries that can be imported in HTML5 WebApps)
- derequire
*/
const exec = require('shelljs').exec;
const echo = require('shelljs').echo;
const codegen = require('../codegen/index.js');
const fs = require('fs');


function remove_trailing_slash(str) {
    var ret = str.replace(/\/$/, "");
    return ret;
}

function build4browserify(pmainjs,pfilename4pkg,pdir4build) {
  var mainjs = pmainjs || "./src/index.js";
  var filename4pkg = pfilename4pkg || './package.json';
  var dir4build = remove_trailing_slash(pdir4build) || './dist';
  // pkg is the required package.js of the repository
  // you want to run the build process in
  var browserify = './node_modules/.bin/browserify';
  var derequire = './node_modules/.bin/derequire';
  var terser = './node_modules/.bin/terser';

  // ok somehow,
  // for deploys, we want the 'browser' field
  // but that messes with browserify...
  // so temporarily remove it during builds.
  // ¯\_(ツ)_/¯
  exec('mv ' + filename4pkg + ' ' + filename4pkg + '.backup');
  delete pkg.browser;
  fs.writeFileSync(filename4pkg, JSON.stringify(pkg, null, 2));


  //final build locations
  var banner = codegen.get_header(pkg);
  var uncompressed = dir4build + "/" + pkg.name + '.js';
  var compressed = dir4build + "/" + pkg.name + '.min.js';

  //cleanup. remove old builds
  exec('rm -rf ' + dir4build + ' && mkdir ' + dir4build);

  //add a header, before our sourcecode
  echo(banner).to(uncompressed);
  echo(banner).to(compressed);

  //browserify + derequire
  var cmd = browserify + ' ' + mainjs + ' --standalone wtf';
  cmd += ' -t [ babelify --presets [ @babel/preset-env ] ]';
  cmd += ' | ' + derequire;
  cmd += ' >> ' + uncompressed;
  exec(cmd);

  //uglify
  cmd = terser + ' ' + uncompressed + ' --mangle --compress ';
  cmd += ' >> ' + compressed;
  exec(cmd);

  //log the size of our builds
  require('./filesize');

  //.. then we replace original package.json file
  exec('mv ' + filename4pkg + '.backup ' + filename4pkg + ' ');

}

module.exports = build4browserify;
